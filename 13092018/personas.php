<?php

function __autoload($nombre_clase) {
    include "personas\\" . $nombre_clase . '.php';
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
//            $clientes=array(); // Es lo mismo que clientes=[]
        $clientes = [];
        $empresas = [];
        $clientes[] = new Cliente([
            "nombre" => "Rosa",
            "edad" => 43
        ]);
        $clientes[0]->mostrar(); // Método de cliente
        echo '<br>';
        $clientes[0]->mostrarPadre();
        echo '<br>';
        $clientes[] = new Cliente([
            "nombre" => "José",
            "edad" => 18,
            "telefono" => "912-912-912",
        ]);
        $clientes[1]->mostrar();
        echo '<br>';
        $clientes[1]->mostrarPadre();
        $empresas[] = new Empresa();
//        Crear nuevo directivo y un empleado
//        Y que pueda pasar a directivo y empleado un array asociativo
        $directivos[] = new Directivo([
            "nombre"=> "Pepe",
            "edad" => 34,
            "categoria"=> "Director",
            "sueldoBruto" => 3500,
        ]);
        echo "<br>";
        $directivos[0]->mostrar();
        echo "<br>";
        $directivos[0]->mostrarPadre();
        echo "<br>";
        $directivos[0]->calcularSalarioNeto();
        $empleados[] = new Empleado([
            "nombre"=> "Eli",
            "edad" => 31,
            "sueldoBruto" => 1800,
        ]);
        echo "<br>";
        $empleados[0]->mostrar();
        echo "<br>";
        $empleados[0]->mostrarPadre();
        echo "<br>";
        $empleados[0]->calcularSalarioNeto();

        ?>
    </body>
</html>
