<?php

/**
 * Description of Directivo
 *
 * @author ramon
 */
class Directivo extends Empleado{
    private $categoria;
    public function mostrar(){
        echo "Soy un directivo y mi categoria es $this->categoria";
    }
    public function tipoEmpleado(){
        parent::mostrar();
    }
    
    function __construct($argumentos=[]) {
        $this->categoria=(isset($argumentos["categoria"]))?$argumentos["categoria"]:"";
        parent::__construct($argumentos);
        
    }

}
