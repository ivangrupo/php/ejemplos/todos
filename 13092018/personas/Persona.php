<?php

/**
 * Description of Persona
 *
 * @author ramon
 */
class Persona {
    private $nombre;
    private $edad;
    public function mostrar(){
        echo "Soy una persona, mi nombre es $this->nombre y mi edad es $this->edad";
    }
    public function __construct($argumentos=[]) {
        $this->nombre=(isset($argumentos["nombre"]))?$argumentos["nombre"]:"";
        $this->edad=(isset($argumentos["edad"]))?$argumentos["edad"]:0;
        
//        Esto daría problemas
//        $this->nombre = $argumentos["nombre"];
//        $this->edad = $argumentos["edad"];
    }

}
