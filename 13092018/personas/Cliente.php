<?php

/**
 * Description of Cliente
 *
 * @author ramon
 */
class Cliente extends Persona {

    private $telefonoContacto;

    public function mostrar() {
    echo "Soy un cliente y mi telefono es $this->telefonoContacto";
    }

    public function mostrarPadre() {
    parent::mostrar();
    }

    public function __construct($argumentos = []) {

        /* if(isset($argumentos["telefono"])){
          $this->telefonoContacto=$argumentos["telefono"];
          }else{
          $this->telefonoContacto="";
          } */

        $this->telefonoContacto = (isset($argumentos["telefono"]))?$argumentos["telefono"]:"";

        // quiero ejecutar el constructor del padre
        parent::__construct($argumentos);
    }

}


// Ejemplo operador ternario con números

/* if($a==10){
  $b=23;
  }else{
  $b=30;
  }

  $b=($a==10)?23:30; */
