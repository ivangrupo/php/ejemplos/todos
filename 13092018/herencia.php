<?php
function __autoload($nombre_clase) {
    /*
     * para que funcione en MAC y Linux
     */
    include "herencia/" . $nombre_clase . '.php';
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        $objeto1 = new Ambulancia();
        $objeto1->pintar();
        //no puedo
        //echo $objeto1->nombre;
        //si puedo
        $objeto1->getNombre();
        $objeto1->pintarPadre();
        

        ?>
    </body>
</html>
