<?php
class Cadena{
    public function __construct($argumento){
        $this->setValor($argumento);
    }
    
    public function getValor($minusculas=false) {
        if($minusculas){
            return strtolower($this->valor);
        }
        return $this->valor;
        
    }

    private function setValor($v) {
        $this->valor = $v;
        $this->setLongitud();
        $this->setVocales();
    }
    
    public function getLongitud() {
        return $this->longitud;
    }

    private function setLongitud() {
        $this->longitud = strlen($this->getValor());
    }
    
    public function getVocales() {
        return $this->vocales;
    }

    private function setVocales() {
        $this->vocales = substr_count($this->getValor(true),"a");
        $this->vocales += substr_count($this->getValor(true),"e");
        $this->vocales += substr_count($this->getValor(true),"i");
        $this->vocales += substr_count($this->getValor(true),"o");
        $this->vocales += substr_count($this->getValor(true),"u");
    }

            
    private $valor;
    private $longitud;
    private $vocales;
    

   
}

