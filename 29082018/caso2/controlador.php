<?php
    
    $formulario=1;
    
    $model=[
        "nombre"=>[
            "valor"=>"",
            "error"=>""
        ],
        "poblacion"=>[
            "valor"=>"",
            "error"=>""
        ],
        "edad"=>[
            "valor"=>"",
            "error"=>""
        ],
        "telefono"=>[
            "valor"=>"",
            "error"=>""
        ],
        "errores"=>""
    ];
    
    
    if (empty($_REQUEST)) {
        $model["errores"]="* Rellena los campos";
    }else if (empty($_REQUEST["nombre"]) && empty($_REQUEST["poblacion"]) && empty($_REQUEST["edad"]) && empty($_REQUEST["telefono"])) {
        $model["errores"]="No has escrito nada, tienes que rellenar todos los campos";
    }else {
        $model["errores"]=[];
        // Comprobar errores
        if (empty($_REQUEST["nombre"])) {
            $model["errores"][]="Introduce tu nombre";
            $model["nombre"]["error"]="* Introduce tu nombre";
        }else{
            $model["nombre"]["valor"]=$_REQUEST["nombre"];
        }
        if(empty($_REQUEST["poblacion"])){
            $model["errores"][]="Introduce tu población";
            $model["poblacion"]["error"]="* Introduce tu población";
        }else{
            $model["poblacion"]["valor"]=$_REQUEST["poblacion"];
        }
        
        if(empty($_REQUEST["edad"])){
            $model["errores"][]="Ingresa tu edad";
            $model["edad"]["error"]="* Ingresa tu edad";
        }else{
            $model["edad"]["valor"]=$_REQUEST["edad"];
        }
        
        if(empty($_REQUEST["telefono"])){
            $model["errores"][]="Introduce tu teléfono";
            $model["telefono"]["error"]="* Introduce tu teléfono";
        }else{
            $model["telefono"]["valor"]=$_REQUEST["telefono"];
        }

        if(count($model["errores"])==0){
            // No hay errores
            // Hay escrito algo en todos los campos
            // Mostrar resultado
            $formulario=0;
        }else{
        }        
    }
    //var_dump(empty($model["errores"]));

?>
        

