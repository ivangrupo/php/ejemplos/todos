<div class="errores">
<?php
    //var_dump($model["errores"]);
    
    if(is_array($model["errores"])){
        echo "<ul>";
        foreach ($model["errores"] as $valor){
            echo "<li>$valor</li>";
        }
        echo "</ul>";
    }else{
        echo $model["errores"];
    }
?>
</div>

<form method="GET">
    <article>
        <section>
            <p><label for="nombre">Nombre</br><span class="margen"></label><input type="text" name="nombre" id="nombre" value="<?= $model["nombre"]["valor"]; ?>" size="30" placeholder="Escribe tu nombre" />
                <span class="error"> <?= $model["nombre"]["error"] ?></span>
            </p></span>
            <p><label for="poblacion">Población</label></br>
                <select name="poblacion" id="poblacion" size="1" />
                    <option value="" selected>Selecciona tu población</option>
                    <?php
                        if($model["poblacion"]["valor"]=="Santander"){
                            echo '<option value="Santander" selected>Santander</option>';
                        }else{
                            echo '<option value="Santander">Santander</option>';
                        }
                    ?>
                    <?php
                        $seleccionado=($model["poblacion"]["valor"]=="Torrelavega")?"selected":"";
                    ?>
                    <option value="Torrelavega" <?= $seleccionado; ?> >Torrelavega</option>
                    <option value="Noja" <?= ($model["poblacion"]["valor"]=="Noja")?"selected":""; ?> >Noja</option>
                </select>
                <?php echo '<span class="error">' . $model["poblacion"]["error"] . '</span>';?>
                </p>
            <p><label for="edad">Edad</br><span class="margen"></label><input type="number" name="edad" id="edad" value="<?= $model["edad"]["valor"]; ?>" size="2" placeholder="Introduce tu edad" />
                <?php echo '<span class="error">' . $model["edad"]["error"] . '</span>';?>
            </p><span>    
            <p><label for="telefono">Teléfono</br><span class="margen"></label><input type="tel" name="telefono" id="edad" value="<?= $model["telefono"]["valor"]; ?>" size="9" placeholder="Núm. Teléfono" />
                <?php echo '<span class="error">' . $model["telefono"]["error"] . '</span>';?>
            </p><span>
                
        </section>
    </article>	
        <p><button/>Enviar</button></p>
</form>



