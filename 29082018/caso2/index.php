<?php    
    include "libreria.php";
    include "controlador.php";
?>

<!DOCTYPE html>

<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Ejemplo 1 PHP caso2</title>
        <link rel="stylesheet" href="1.css">
    </head>
    <body>
	
        <?php
            
            if($formulario){
                include "formulario.php";
            }else{
                include "resultados.php";
            }
        
        ?>
        
    </body>
</html>
