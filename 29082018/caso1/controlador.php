<?php
    
    $formulario=1;

    $model=[
        "nombre"=>"",
        "poblacion"=>"",
        "edad"=>null,
        "telefono"=>null,
        "errores"=>[], // o "errores"=>"", si se trata como string
    ];
    
    if (empty($_REQUEST)) {
        // Carga el formulario por primera vez
        $model["errores"]="* Rellena todos los campos";
    }else if (empty($_REQUEST["nombre"]) && empty($_REQUEST["poblacion"]) && empty($_REQUEST["edad"]) && empty($_REQUEST["telefono"])) {
        $model["errores"]="* Rellena los campos";
    }else {
        $model["errores"]=[];
        // Comprobar errores
        if (empty($_REQUEST["nombre"])) {
            $model["errores"][]="* Introduce tu nombre";
            
        }else{
            $model["nombre"]=$_REQUEST["nombre"];
        }
        if (empty($_REQUEST["poblacion"])) {
            $model["errores"][]="* Introduce tu población";
            
        }else{
            $model["poblacion"]=$_REQUEST["poblacion"];
        }
        if (empty($_REQUEST["edad"])) {
            $model["errores"][]="* Introduce tu edad";
            
        }else{
            $model["edad"]=$_REQUEST["edad"];
        }
        if (empty($_REQUEST["telefono"])) {
            $model["errores"][]="* Introduce tu teléfono";
            
        }else{
            $model["telefono"]=$_REQUEST["telefono"];
        }
        if(count($model["errores"])==0){
            // No hay errores, todos los campos rellenados, muestra el resultado
            $formulario=0;
        }else {
        }
    }
    
    //var_dump(empty($model["errores"]));

?>