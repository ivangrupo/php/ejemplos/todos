<?php
        function  actionResultado(){
            $frase =$_REQUEST["frase"];
            
            $contador=[];
            $contador["a"]=substr_count(strtolower($frase), "a");
            $contador["e"]=substr_count(strtolower($frase), "e");
            $contador["i"]=substr_count(strtolower($frase), "i");
            $contador["o"]=substr_count(strtolower($frase), "o");
            $contador["u"]=substr_count(strtolower($frase), "u");
            
            return [
                "titulo"=>"Calcular el numero de vocales",
                "content"=>"vocales.php",
                "curso"=>"Año 2018",
                "pie"=>"Resultados de la aplicacion",
                "nVocales"=>$contador
            ];
        }
        
        function actionIndex(){
            return [
                "titulo"=>"Calcular el numero de vocales",
                "content"=>"formulario.php",
                "curso"=>"Año 2018",
                "pie"=>"Pagina principal de la aplicacion",
                "mensaje"=>"introduce una frase",
            ];
        }

        
        if (isset($_REQUEST["enviar"])) {
           $datos=actionResultado();
        }else{
            $datos=actionIndex();
        }


