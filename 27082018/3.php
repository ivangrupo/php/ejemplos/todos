<!DOCTYPE html>

<?php

    $bien=1;    // Los datos llegan bien
    
    if(empty($_REQUEST)){
        $bien=0;    // Los datos no llegan
    }

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejemplo3</title>
    </head>
    <body>
        <div id="wrapper">
            <?php
                if ($bien==0) {
            ?>
		<header>
			<h1>FICHA DE TRANSPORTE</h1>
		</header>
		<article>
			<section>
                            <form action="3.php" method="get" accept-charset="utf-8">
                                <p><label for="nombre">Nombre</br><span class="margen"></label><input type="text" name="nombre" id="nombre" size="30" /></p></span>
                                <p><label for="apellidos">Apellidos</br><span class="margen"></label><input type="text" name="apellidos" id="apellidos" size="30" /></p></span>
                                <p><label for="edad">Edad</br><span class="margen"></label><input type="number" name="edad" id="edad" size="2" /></p><span>
                                <p><label for="poblacion">Población</label></br>
                                    <select name="poblacion" id="poblacion" size="1" />
					<option selected></option>
					<option>Santander</option>
					<option>Torrelavega</option>
					<option>Noja</option>
					<option>Reinosa</option>
					<option>Potes</option>
					<option>Otro</option>
                                    </select>
                                </p>
			</section>
			<aside class="left">	
                            <h2><label>Barrio</h2>
                            <p><input type="radio" name="barrio" value="Norte" id="norte" checked="norte">Norte</p>
                            <p><input type="radio" name="barrio" value="Sur" id="sur">Sur</p>
                            <p><input type="radio" name="barrio" value="Este" id="este">Este</p>
                            <p><input type="radio" name="barrio" value="Oeste" id="oeste">Oeste</label></p>
				
                            <p><label>Medio</br>
                            <p><span class="margen2"><label for="bus">Bus</label><input type="checkbox" name="medio[]" value="bus" id="bus" /><span>
                            <span class="margen2"><label for="tren">Tren</label><input type="checkbox" name="medio[]" value="tren" id="tren" /><span>
                            <span class="margen2"><label for="coche">Coche</label><input type="checkbox" name="medio[]" value="coche" id="coche" /><span>
                            <span class="margen2"><label for="moto">Moto</label><input type="checkbox" name="medio[]" value="moto" id="moto" /></span></p>
				
			</aside>
		</article>
		<footer>	
                    <p><button/>Enviar</button></p>
			</form>
                            
                            <?php
                                }else {
                                    echo "<p>";
                                echo "Nombre: ";
                                echo $_REQUEST['nombre'];
                                echo "</p>";
                                echo "<p>";
                                echo "Apellidos: ";
                                echo $_REQUEST['apellidos'];
                                echo "</p>";
                                echo "<p>";
                                echo "Edad: ";
                                echo $_REQUEST['edad'];
                                echo "</p>";
                                echo "<p>";
                                echo "Población: ";
                                echo $_REQUEST['poblacion'];
                                echo "</p>";
                                echo "<p>";
                                echo "Barrio: ";
                                echo $_REQUEST['barrio'];
                                echo "</p>";
                                echo "<p>";
                                echo "Medio de transporte: ";
                                if(isset($_REQUEST['medio'])){
                                    foreach ($_REQUEST['medio'] as $valor) {
                                        echo $valor;
                                    }
                                }else{
                                    echo "No has seleccionado un medio de transporte";
                                }
                                echo "</p>";
                                
                                var_dump($_REQUEST);
                                }
                            ?>
		</footer>
	</div>
    </body>
</html>
