<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejemplo2</title>
    </head>
    <body>
        
        <?php
        
            var_dump($_REQUEST);
        
        ?>
        
        <?php
            
            if (!empty($_REQUEST)) {
                
                echo "<p>";
                echo "Nombre: ";
                echo $_REQUEST['nombre'];
                echo "</p>";
                echo "<p>";
                echo "Apellidos: ";
                echo $_REQUEST['apellidos'];
                echo "</p>";
                echo "<p>";
                echo "Edad: ";
                echo $_REQUEST['edad'];
                echo "</p>";
                echo "<p>";
                echo "Población: ";
                echo $_REQUEST['poblacion'];
                echo "</p>";
                echo "<p>";
                echo "Barrio: ";
                echo $_REQUEST['barrio'];
                echo "</p>";
                echo "<p>";
                echo "Medio de transporte: ";
                if(isset($_REQUEST['medio'])){
                    foreach ($_REQUEST['medio'] as $valor) {
                        echo $valor;
                    }
                }else{
                    echo "No has seleccionado un medio de transporte";
                }
                echo "</p>";
            
            }else{
                echo "No has introducido datos";
            }
         
        ?>
    </body>
</html>
