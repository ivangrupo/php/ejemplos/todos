<!DOCTYPE html>

<?php

    $bien=1;    // Los datos llegan bien
    
    if(empty($_REQUEST)){
        $bien=0;    // Los datos no llegan
    }

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejemplo3</title>
    </head>
    <body>
        <div id="wrapper">
            <?php
                // Cargar formulario
                if ($bien==0) {
                    include 'formulario.php';
                } else {
                // Mostrar resultados
                    include 'resultados.php';
                }
            ?>
		
	</div>
    </body>
</html>
