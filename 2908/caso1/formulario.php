<div>
<?php
    echo $modelo["errores"];
?>
</div>

<form method="get">
    <div>
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" id="nombre" value="<?= $modelo["nombre"]; ?>" placeholder="Escribe tu nombre">
    </div>
    <div>
        <label for="nombre">Poblacion</label>
        <select name="poblacion" id="poblacion">
            <option>Selecciona la poblacion</option>
            <option value="torrelavega">Torrelavega</option>
            <option value="laredo">Laredo</option>
            <option value="potes">Potes</option>
        </select> 
    </div>
    <div>
        <label for="edad">Edad</label>
        <input type="number" name="edad" id="edad" value="<?= $modelo["edad"]; ?>" placeholder="Escribe la edad">
    </div>
    <div>
        <label for="telefono">Telefono</label>
        <input type="text" name="telefono" id="telefono" value="<?= $modelo["telefono"]; ?>" placeholder="escribe tu telefono">
    </div>
    <button>Enviar</button>
</form>

