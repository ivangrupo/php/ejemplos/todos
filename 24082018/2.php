<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ej2 PHP <?= date("d/m/Y"); ?>
        </title>
        <!-- Mejor hacerlo así, sin el echo -->
    </head>
    <body>
        <?php
            // var_dump($_GET);    /* Recibe los datos si se utiliza get */
            // var_dump($_POST);   /* Recibe los datos si se utiliza post */ 
            // var_dump($_REQUEST); /* Recibe los datos siempre */
            
            echo $_REQUEST['n1'] + $_REQUEST['n2'];
        ?>
    </body>
</html>