<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ej3 PHP <?= date("d/m/Y"); ?>
        </title>
        <!-- Mejor hacerlo así, sin el echo -->
    </head>
    <body>

        <form action="4.php" method="get" accept-charset="utf-8">
            <div>
                <input type="text" name="n1">N1
            </div>
            <div>
                <input type="text" name="n1">N2
            </div>
            <div>
                <input type="radio" name="operacion" value="suma">Sumar
                <input type="radio" name="operacion" value="resta">Restar
            </div>
            <div>
                <button>CALCULAR</button>
            </div>
        </form>

    </body>
</html>