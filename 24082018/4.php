<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ej4 PHP <?= date("d/m/Y"); ?>
        </title>
        <!-- Mejor hacerlo así, sin el echo -->
    </head>
    <body>

        <?php
            if (empty($_REQUEST)) {
                echo "No tengo nada que hacer";
            }else{

                if ($_REQUEST['operacion']=="suma") {
                    echo $_REQUEST['n1'] + $_REQUEST['n2'];
                }else{
                    echo $_REQUEST['n1'] - $_REQUEST['n2'];
                }
            }
        ?>
        
    </body>
</html>