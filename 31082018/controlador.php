<?php
    
    include "model.php";
    $errores=[];

    // var_damp($_REQUEST);
    
    if (empty($_REQUEST)) {
        // Carga el formulario por primera vez
        echo "Acción 1";
        $aplicacion["vista"]="formulario.php";
    }else if (empty($_REQUEST["nombre"]) && empty($_REQUEST["poblacion"]) && empty($_REQUEST["edad"]) && empty($_REQUEST["telefono"])) {
        echo "Acción 2";
        $aplicacion["vista"]="formulario.php";
        $errores="* Rellena los campos";
    }else {
        leerDatos($campos);
        $errores=comprobar($campos);
        
        // Comprobar errores
        
        if(count($errores)==0){
            // No hay errores, todos los campos rellenados, muestra el resultado
            echo "Acción 4";
            $aplicacion["vista"]="resultados.php";
        }else {
            echo "Acción 3";
            $aplicacion["vista"]="formulario.php";
        }
    }
    
    //var_dump(empty($model["errores"]));

    
?>