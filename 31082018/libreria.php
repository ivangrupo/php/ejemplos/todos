<?php

/**
 * 
 * @param type $m   // Recoge la variable $m por valor, que es la variable $campos (que es un array de arrays)
 */
function leerDatos($m){
    foreach ($m as $indice => $valor) {
        $m[$indice]["value"]=$_REQUEST["$indice"];
    }
}


/**
 * 
 * @param type $m   // Recoge la variable $m por referencia, que es la variable $campos (que es un array de arrays)
 * 
 * USAR ESTE MÉTODO SI QUIERES MODIFICARLO
 */

/*

function leerDatos(&$m){
    foreach ($m as $indice => &$valor) {
        $valor["value"]=$_REQUEST["$indice"];
    }
}

 */


/**
 * 
 * @param type $m
 * @return type
 */
function comprobar($m){
    $errores=[];
    foreach ($m as $indice => $valor) {
        if (empty($valor["value"])){
            $errores[$indice]=$valor["mserror"];
        }
    }
    return $errores;
}

function dibujarControl($datos,$errores,$nombre){
    
    echo "<p>";
    echo '<label for="' . $nombre . '">' . $datos["label"] . '</br></label>';
    if ($datos["type"]=="select") {
        
    }else{
        echo '<input type="' . $datos["type"] . '" name="' . $nombre . '" id="' . $nombre . '" value="' . $datos["value"] . '" placeholder="' . $datos["placeholder"] .'">';
    }
    echo "</p>";
    echo "</span>";
}

?>