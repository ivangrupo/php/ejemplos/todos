<?php
    
    $campos=[
        
        "nombre"=>[
            "label"=>"Nombre Completo",
            "type"=>"text",
            "value"=>"",
            "placeholder"=>"Escribe tu nombre",
            "mserror"=>"El nombre no puede estar vacío",
        ],
        
        "poblacion"=>[
            "label"=>"Población",
            "type"=>"select",
            "value"=>"",
            "placeholder"=>"Selecciona tu población",
            "mserror"=>"Debes seleccionar una población",
        ],
        
        "edad"=>[
            "label"=>"Edad",
            "type"=>"number",
            "value"=>"",
            "placeholder"=>"Introduce tu edad",
            "mserror"=>"La edad no puede estar vacía",
        ],
        
        "telefono"=>[
            "label"=>"Teléfono",
            "type"=>"tel",
            "value"=>"",
            "placeholder"=>"Introduce tu teléfono",
            "mserror"=>"El teléfono no puede estar vacío",
            "options"=>["Santander","Torrelavega","Noja"],
        ],
    ];

?>

