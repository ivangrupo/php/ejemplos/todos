<div>
<?php
    //var_dump($modelo["errores"]);
    
    if(is_array($modelo["errores"])){
        echo "<ul>";
        foreach ($modelo["errores"] as $valor){
            echo "<li>$valor</li>";
        }
        echo "</ul>";
    }else{
        echo $modelo["errores"];
    }
?>
</div>

<form method="get">
    <div>
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" id="nombre" value="<?= $modelo["nombre"]; ?>" placeholder="Escribe tu nombre">
    </div>
    <div>
        <label for="nombre">Poblacion</label>
        <select name="poblacion" id="poblacion">
            <option value="">Selecciona la poblacion</option>
            <?php
                if($modelo["poblacion"]=="torrelavega"){
                    echo '<option value="torrelavega" selected>Torrelavega</option>';
                }else{
                    echo '<option value="torrelavega">Torrelavega</option>';
                }
            ?>
            
            <?php
                $seleccionado=($modelo["poblacion"]=="laredo")?"selected":"";
            ?>
            <option value="laredo" <?= $seleccionado; ?>>Laredo</option>
            
            <option value="potes" <?= ($modelo["poblacion"]=="potes")?"selected":""; ?>>Potes</option>
        </select> 
    </div>
    <div>
        <label for="edad">Edad</label>
        <input type="number" name="edad" id="edad" value="<?= $modelo["edad"]; ?>" placeholder="Escribe la edad">
    </div>
    <div>
        <label for="telefono">Telefono</label>
        <input type="text" name="telefono" id="telefono" value="<?= $modelo["telefono"]; ?>" placeholder="escribe tu telefono">
    </div>
    <button>Enviar</button>
</form>

