<?php
    include "config.php";
    require_once "controller.php";
?>

<!DOCTYPE html>

<html>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title><?=$aplicacion["titulo"];?></title>
  </head>
  <body>
    <div class="container">
        
        <div class="card-header">
            <div class="container-fluid">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark text-danger">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                      <ul class="navbar-nav mr-auto">
                        <li class="nav-item <?=($aplicacion["vista"]==0)? "active":"";?>">
                          <a class="nav-link" href="index.php">Inicio <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item <?=($aplicacion["vista"]==1)? "active":"";?>">
                          <a class="nav-link" href="index.php?view=1">Ejemplo 1</a>
                        </li>
                        <li class="nav-item <?=($aplicacion["vista"]==2)? "active":"";?>">
                          <a class="nav-link" href="index.php?view=2">Ejemplo 2</a>
                        </li>
                        <li class="nav-item <?=($aplicacion["vista"]==3)? "active":"";?>">
                          <a class="nav-link" href="index.php?view=3">Ejemplo 3</a>
                        </li>
                        <li class="nav-item <?=($aplicacion["vista"]==4)? "active":"";?>">
                          <a class="nav-link" href="index.php?view=4">Ejemplo 4 (Galería)</a>
                        </li>
                      </ul>
                    </div>
                </nav>
          </div>
        </div>

        <div class="container mt-3">
            <?php
                include $aplicacion["vista"];
            ?>
        </div>

        <div class="card mt-3">
            <div class="card-body mx-auto">
                <h5 class="card-title"><?=$aplicacion["pie"];?></h5>
            </div>
        </div>
        
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
