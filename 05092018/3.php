<style>
    p{
        display: block;
        margin: auto;
        text-align: center;
    }
</style>

<?php

    $alumnos=[
        
            [
                "Nombre"=>"Ana",
                "Edad"=>23,
            ],

            [
                "Nombre"=>"Luis",
                "Edad"=>41,
            ],

            [
                "Nombre"=>"Pedro",
                "Edad"=>30,
            ],
        
            [
                "Nombre"=>"Rubén",
                "Edad"=>27,
            ],
                
            [
                "Nombre"=>"Iván",
                "Edad"=>32,
            ],
        ];
        
        foreach ($alumnos as $registro) {
            echo "<div>";
            echo "<p>";
            foreach ($registro as $nCampo => $valor) {
                echo "<br>$nCampo: " . $valor;
            }
            echo "</div>";
        }
        
        var_dump($alumnos);

