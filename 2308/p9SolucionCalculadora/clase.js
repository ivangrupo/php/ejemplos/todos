function realizarOperacion(n1,n2,op){
	var resultado=0;

	n1=Number(n1);
	n2=Number(n2);

	switch(op) {
		case '+':
			resultado=n1+n2;
			break;
		case '-':
			resultado=n1-n2;
			break;
		case '*':
			resultado=n1*n2;
			break;
		case '/':
			resultado=n1/n2;
			break;
	}

	return resultado;
}


// var borrar=false;
// var operacion=0;
// var resultado=0;

var global={
	borrar:true,
	operacion:0,
	resultado:0
};

window.addEventListener("load",(event)=>{
	var operaciones=document.querySelectorAll(".operacion");
	var numeros=document.querySelectorAll(".numero");
	var pantalla=document.querySelector(".display");

	for (var i = 0; i<numeros.length; i++) {
		numeros[i].addEventListener("click",(event)=>{
			if(global.borrar){
				global.resultado=Number(pantalla.value);
				pantalla.value=event.target.value;
				global.borrar=false;
			}else{
				pantalla.value+=event.target.value;
			}
		});
	}

	operaciones.forEach((boton)=>{
		boton.addEventListener("click",(event)=>{
			if(event.target.value!="="){
				if(global.operacion==0){
					global.resultado=Number(pantalla.value);
				}else{
					global.resultado=realizarOperacion(global.resultado,pantalla.value,global.operacion);
					// funcion autoejecutable 
					// global.resultado=function(){
							// codigo de la funcion realizarOperacion
					// }(global.resultado,pantalla.value,global.operacion);
				}
				global.borrar=true;
				global.operacion=event.target.value;
				pantalla.value=global.resultado;
			}else{
				pantalla.value=realizarOperacion(global.resultado,pantalla.value,global.operacion);
				global.borrar=true;
				global.operacion=0;
				global.resultado=0;
			}
		});
	});
});
