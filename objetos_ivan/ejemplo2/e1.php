<?php
function __autoload($nombre_clase) {
    include $nombre_clase . '.php';         // UTILIZAR SIEMPRE PARA CARGAR AUTOMÁTICAMENTE LAS CLASES UTILIZANDO SU NOMBRE (en este ejemplo se carga la clase Vector)
}
?>
<!DOCTYPE html>
<!--
Ejemplo 2016
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $objeto=new Vector([1,2,45,1,2,3,44]);

            echo "La moda es: " . $objeto->getModa();
            echo "<br>";
            echo "El máximo es: " . $objeto->getMaximo();
            echo "<br>";
            echo "Los valores introducidos son: " . $objeto->getCadena();
        ?>
    </body>
</html>
