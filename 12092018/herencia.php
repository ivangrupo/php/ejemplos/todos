<?php
function __autoload($nombre_clase) {
    // include "herencia\\" . $nombre_clase . '.php';      // EN ESTE CASO AL NO ESTAR EN LA MISMA CARPETA HAY QUE INDICARLA (Sólo funciona en windows)
    include "herencia/" . $nombre_clase . '.php';       // Funciona en todos los SO
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        $objeto1 = new Ambulancia();
        $objeto1->pintar();
        //echo $objeto1->nombre;    Es privado y no se puede llamar así
        echo "<br>";
        echo $objeto1->getNombre();
        echo "<br>";

        ?>
    </body>
</html>
