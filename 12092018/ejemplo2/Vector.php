<?php

class Vector {
    
    public function __construct($valores) {
        $this->valores = $valores;
        $this->setModa();
        $this->setMaximo();
    }

    public function setMaximo() {
        $this->maximo = max($this->getValores());
    }
    
    public function getMaximo() {
        return $this->maximo;
    }
        
    public function getValores() {
        return $this->valores;
    }
    
    public function imprimirValores(){
        var_dump($this->getValores());
    }

    public function setValores($valores) {
        $this->valores = $valores;
        $this->setModa();
        $this->setMaximo();
    }
    
    public function setModa(){
        $this->moda=1;
    }
    
    public function getModa(){
        return $this->moda;
    }
    
    private function setCadena() {
        $this->cadena=implode("-", $this->valores);
    }
    
    public function getCadena() {
        $this->setCadena();     // También se podría llamar en el constructor (es mejor)
        return $this->cadena;
    }

        private $valores;
        private $moda;
        private $maximo;
        private $cadena;
    
}
