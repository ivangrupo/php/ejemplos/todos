<?php
    function __autoload($nombre_clase) {
        include $nombre_clase . '.php';         // UTILIZAR SIEMPRE PARA CARGAR AUTOMÁTICAMENTE LAS CLASES UTILIZANDO SU NOMBRE (en este ejemplo se carga la clase Cadena)
    }
?>


<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Clase Cadena</title>
    </head>
    <body>
        <?php
            // Vamos a crear un objeto de tipo cadena
            $texto = new Cadena("Ejemplo"); // Instanciamos la clase Cadena
            echo "La frase introducida es: " . $texto->getValor();
            echo "<br>";
            echo "La frase introducida es: " . $texto->getValor(true);
            echo "<br>";
            echo "La longitud es: " . $texto->getLongitud();
            echo "<br>";
            echo "El nº de vocales es: " . $texto->getVocales();
        ?>
    </body>
</html>
