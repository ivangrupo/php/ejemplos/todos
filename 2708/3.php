<!DOCTYPE html>
<?php
    $bien=1; // supongo que los datos me llegan
        
    if(empty($_REQUEST)){
        $bien=0; // los datos no me llegan
    }
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="wrapper">
        <?php
            if($bien==0){
        ?>
            <form method="get">
                <div>
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" id="nombre">
                </div>
                <div>
                    <label for="apellidos">Apellidos</label>
                    <input type="text" name="apellidos" id="apellidos">
                </div>
                <div>
                    <label for="edad">Edad</label>
                    <input type="number" name="edad" id="edad">
                </div>
                <div>
                    <label for="poblacion">Poblacion</label>
                    <select name="poblacion" id="poblacion">
                        <option value="santander">Santander</option>
                        <option value="potes">Potes</option>
                    </select>
                </div>

                <div>
                    <div>Barrio</div>
                    <input type="radio" name="barrio" id="norte" value="Norte" >
                    <label for="norte">Norte</label>
                    <input type="radio" name="barrio" id="sur" value="Sur" >
                    <label for="sur">Sur</label>
                    <input type="radio" name="barrio" id="este" value="Este" >
                    <label for="este">Este</label>
                    <input type="radio" name="barrio" id="oeste" value="Oeste" >
                    <label for="oeste">Oeste</label>

                </div>
                <div>
                    <div>Medio</div>
                    <input type="checkbox" name="medio[]" id="bus" value="bus">
                    <label for="bus">Bus</label>
                    
                    <input type="checkbox" name="medio[]" id="tren" value="tren">
                    <label for="tren">Tren</label>
                    <input type="checkbox" name="medio[]" id="moto" value="moto">
                    <label for="moto">Moto</label>
                    <input type="checkbox" name="medio[]" id="coche" value="coche">
                    <label for="coche">Coche</label>
                    
                </div>

                <button>Enviar</button>
            </form>
        <?php
            }else{
                echo "El nombre es :" . $_REQUEST['nombre'];
                echo "<br>";
                echo "Los apellidos son: {$_REQUEST['apellidos']}";
                echo "<br>";
                echo "La edad es: " ;
                echo $_REQUEST["edad"];
                echo "<br>";
                echo "La población es: " . $_REQUEST["poblacion"];
                echo "<br>";
                echo "El barrio es: " . $_REQUEST["barrio"];
                echo "<br>";
                echo "Los medios seleccionados son: ";
                // echo $_REQUEST["medio"];
                if(isset($_REQUEST["medio"])){
                    foreach($_REQUEST["medio"] as $valor){
                        echo "<br>$valor";
                    }
                }else{
                    echo "No has seleccionado ningun medio";
                }
            }
        ?>

        </div>
        <?php
        ?>
    </body>
</html>