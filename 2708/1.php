<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="wrapper">
            <form action="2.php" method="get">
                <div>
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" id="nombre">
                </div>
                <div>
                    <label for="apellidos">Apellidos</label>
                    <input type="text" name="apellidos" id="apellidos">
                </div>
                <div>
                    <label for="edad">Edad</label>
                    <input type="number" name="edad" id="edad">
                </div>
                <div>
                    <label for="poblacion">Poblacion</label>
                    <select name="poblacion" id="poblacion">
                        <option value="santander">Santander</option>
                        <option value="potes">Potes</option>
                    </select>
                </div>

                <div>
                    <div>Barrio</div>
                    <input type="radio" name="barrio" id="norte" value="Norte" >
                    <label for="norte">Norte</label>
                    <input type="radio" name="barrio" id="sur" value="Sur" >
                    <label for="sur">Sur</label>
                    <input type="radio" name="barrio" id="este" value="Este" >
                    <label for="este">Este</label>
                    <input type="radio" name="barrio" id="oeste" value="Oeste" >
                    <label for="oeste">Oeste</label>

                </div>
                <div>
                    <div>Medio</div>
                    <input type="checkbox" name="medio[]" id="bus" value="bus">
                    <label for="bus">Bus</label>
                    
                    <input type="checkbox" name="medio[]" id="tren" value="tren">
                    <label for="tren">Tren</label>
                    <input type="checkbox" name="medio[]" id="moto" value="moto">
                    <label for="moto">Moto</label>
                    <input type="checkbox" name="medio[]" id="coche" value="coche">
                    <label for="coche">Coche</label>
                    
                </div>

                <button>Enviar</button>
            </form>

        </div>
        <?php
        ?>
    </body>
</html>