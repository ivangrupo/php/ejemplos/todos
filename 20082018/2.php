<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ej2 PHP <?= date("d/m/Y"); ?>
        </title>
        <!-- Mejor hacerlo así, sin el echo -->
    </head>
    <body>
        
        <?php 
            for ($i=1; $i <=10; $i++) { 
                echo "<div>" . $i . "</div>";
            }
        ?>

        <!-- echo "<div>$i</div>";  Es lo mismo, el echo concatena igual así sin los puntos. -->
        
        <br>

        <?php 
            for ($i=1; $i <=10; $i++) { 
        ?>
            <div><?php $i; ?></div>
        <?php
            } 
        ?>
        
        <br>

        <script>
            
            for (var i = 0; i <=10; i++) {
                document.write("<div>" + i + "</div>");
            }

        </script>
    </body>
</html>