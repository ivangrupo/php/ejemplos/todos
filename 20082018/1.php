<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ej1 PHP <?= date("d/m/Y"); ?>
        </title>
        <!-- Mejor hacerlo así, sin el echo -->
    </head>
    <body>
        <?php
            echo "Hola mundo";
        ?>
        <!-- Es lo mismo -->
        <?= "<div>Hola mundo dentro de un div</div>"; ?>    <!-- Mejor utilizar html -->
        <div>
            <?= "Hola mundo dentro de un div HTML" ?>
        </div>

    </body>
</html>