<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ej4 PHP <?= date("d/m/Y"); ?>
        </title>
        <!-- Mejor hacerlo así, sin el echo -->
    </head>
    <body>

        <?php
            /* creamos la variable */
            $texto = 'Ejemplo de clase';

            /* manejar funciones del string */
            for ($i=0; $i < strlen($texto); $i++) { 
                echo "<li>" . $texto[$i] . "</li>";
            }
        ?>
        
    </body>
</html>