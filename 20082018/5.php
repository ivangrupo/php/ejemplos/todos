<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ej5 PHP <?= date("d/m/Y"); ?>
        </title>
        <!-- Mejor hacerlo así, sin el echo -->
    </head>
    <body>

        <?php
            /* creamos la variable */
            $texto = 'Ejemplo de clase';

            /* convertir el string en un array */
            $vector=str_split($texto);

            /* manejar funciones del array */
            for ($i=0; $i < count($vector); $i++) { 
                echo "<li>" . $vector[$i] . "</li>";
            }
        ?>
        
    </body>
</html>