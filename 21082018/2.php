<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ej2 PHP <?= date("d/m/Y"); ?>
        </title>
        <!-- Mejor hacerlo así, sin el echo -->
    </head>
    <body>

        <!-- Sustituir las vocales de un string por un guión -->
        
        <?php

            function vocal($caracter){
                // $caracter=strtolower($caracter);
                switch (strtolower($caracter)) {
                    case 'a':
                    case 'e':
                    case 'i':
                    case 'o':
                    case 'u':
                        return TRUE;
                        break;
                    
                    default:
                        return FALSE;
                        break;
                }

            }

            /* creamos la variable */
            $frase = 'El abecedario';

            /* manejar funciones del string */
            for ($i=0; $i <strlen($frase); $i++) {
                if (vocal($frase[$i])) {
                    $frase[$i]="-";
                }
                
            }

            echo $frase;
            echo "<p>";

            /**/

            print_r($frase);
            echo "<p>";

            /* Para pruebas, muestra el tipo y contenido del elemento*/

            var_dump($frase);
            echo "<p>";

            // Ejemplo
            
            $array=[0,0,1,7];

            var_dump($array);
            echo "<p>";


        ?>

    </body>
</html>