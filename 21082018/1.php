<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ej1 PHP <?= date("d/m/Y"); ?>
        </title>
        <!-- Mejor hacerlo así, sin el echo -->
    </head>
    <body>
        <ul>
            
            <!-- Listar nº pares del 1 al 100 en li -->

            <?php

                for ($i=2; $i<=100; $i+=2){
                    echo "<li>" . $i . "</li>";
                }

                // Hace lo mismo, concatena igual

                for ($i=2; $i<=100; $i+=2){
                    echo "<br>";
                    echo "<li>$i</li>";
                }

                // Lo mismo pero con while

                $num = 0;

                while ($num <= 100){
                    $num++;
                    if ($num % 2 == 0) // o if (!($num % 2))
                      {
                        echo "<br>";
                        echo "<p>$num</p>";
                      }

                }

            ?>
        </ul>
    </body>
</html>