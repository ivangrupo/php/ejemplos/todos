<?php
    include "configuracion.php";
    include "libreria.php";
    include "controlador.php";
?>


<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title><?= $aplicacion["titulo"]; ?></title>
    </head>
    <body>
        <?php
            include $aplicacion["vista"];
        ?>
    </body>
</html>
